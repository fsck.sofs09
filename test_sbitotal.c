#include <stdio.h>

#include "sofs09.h"
#include "fsck.h"
#include "fsck_helper.h"

testresult_t test_sbitotal (void)
{
    testresult_t ret;
    SOSuperBlock *sb;
    int itshortage, itexcess;

    fetch_superblock(&sb);

    /* ceil(a/b) = (a + b - 1)/b */

    ret = nice;
    if (sb->itable_size < (sb->itotal+IPB-1)/IPB) {
        itshortage = (sb->itotal+IPB-1)/IPB - (sb->itable_size);
        printf("Inode table too small by %d blocks.\n", itshortage);
        ret = corrupt;
    }
    if (sb->itable_size > (sb->itotal+IPB-1)/IPB) {
        itexcess = (sb->itable_size) - (sb->itotal+IPB-1)/IPB;
        printf("Inode table too big by %d blocks.\n", itexcess);
        ret = weird;
    }

    return ret;
}
