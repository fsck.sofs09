#include <stdio.h>

#include "sofs09.h"
#include "fsck.h"
#include "fsck_helper.h"

testresult_t test_clusterlost(void)
{
    testresult_t ret;
    cc_t ccstat;
    int r;
    SOSuperBlock *sb;

    ret = nice;
    fetch_superblock(&sb);
    
    for (r = 0; r < sb->dzone_size; ++r) {
        cctable_get(r, &ccstat);
        if (ccstat == bah) {
            printf("Cluster number %d is lost.\n", r);
            ret = corrupt;
        }
    }

    return ret;
}
