#include "sofs09.h"
#include "fsck.h"
#include "fsck_helper.h"

static bool check_cluster(uint32_t pcn, uint32_t cn, uint32_t inodeidx)
{
    bool ret;
    uint32_t cidx;
    uint32_t ciurefs[RPB];
    cc_t ccstat;
    SOSuperBlock *sb;
    uint32_t clucount;

    fetch_superblock(&sb);
    ret = true;
    clucount = 0;

    if (pcn != NULL_BLOCK) {
        clucount++;

        /* check alignment */
        if ((pcn-(sb->dzone_start))%(BLOCKS_PER_CLUSTER)) {
            printf("Cluster number %d of inode number %d points to "
                    "unaligned block.\n", cn, inodeidx);
            return false;
        }

        /* overflow is irrelevant here */
        /* check if cluster address is in bounds */
        cidx = (pcn-(sb->dzone_start))/(BLOCKS_PER_CLUSTER);
        if (cidx >= sb->dzone_size) {
            printf("Cluster number %d of inode number %d points to a place "
                    "outside data zone.\n", cn, inodeidx);
            return false;
        }

        /* check CIUT */
        soReadRawBlock(sb->ciutable_start+(cidx/RPB), ciurefs);
        if (ciurefs[cidx%RPB] != inodeidx) {
            printf("Entry in CIUT for cluster number %d points to inode "
                    "%d, bot it is used by inode %d.\n", cidx,
                    ciurefs[cidx%RPB], inodeidx);
            return false;
        }

        /* check cctable */
        cctable_get(cidx, &ccstat);
        switch (ccstat) {
            case bah:
                cctable_set(cidx, busy);
                break;
            case idle:
                printf("Cluster number %d is used by inode %d but is also "
                        "a free cluster.\n", cidx, inodeidx);
                return false;
                break;
            case busy:
                printf("Cluster %d is used several times by inode %d.\n",
                        cidx, inodeidx);
                return false;
                break;
            default:
                printf("BUM! parse_clusters: cctable is corrupt.\n");
                return false;
                break;
        }
    }
    
    return ret;
}
bool parse_clusters(uint32_t inodeidx)
{
    bool ret;
    uint32_t cnmax;
    uint32_t pcn;
    uint32_t fret;
    uint32_t irefs[RPC];
    uint32_t iirefs[RPC];
    int i1,i2;
    SOSuperBlock *sb;
    SOInode inode;

    fetch_superblock(&sb);
    ret = true;

    fret = soReadInode(&inode, inodeidx);
    if (fret < 0) FABORT(fret, "parse_cluster");

    /* check data clusters */
    cnmax = (inode.size + CLUSTER_SIZE - 1)/CLUSTER_SIZE;
    for (i1 = 0; i1 < cnmax; i1++) {
        fret = soHandleFileCluster(inodeidx, i1, OP_GET, &pcn);
        if (fret < 0) FABORT(fret, "parse_cluster");

        if (check_cluster(pcn, i1, inodeidx) == false) {
            return false;
        }
    }

    /* check reference clusters */

    if (cnmax > N_DIRECT) {
        if (check_cluster(inode.i1, -1, inodeidx) == false) {
            return false;
        }
        fret = soReadCacheCluster(inode.i1, irefs);
        if (fret < 0) FABORT(fret, "parse_cluster");
        for (i1 = 0; (i1 < cnmax-N_DIRECT) && (i1 < RPC); ++i1) {
            if (check_cluster(irefs[i1], -1, inodeidx) == false) {
                return false;
            }
        }
    }

    if (cnmax > (N_DIRECT + RPC)) {
        if (check_cluster(inode.i2, -1, inodeidx) == false) {
            return false;
        }
        fret = soReadCacheCluster(inode.i2, iirefs);
        if (fret < 0) FABORT(fret, "parse_cluster");
        for (i2 = 0; (i2 < RPC) && (i2*RPC < cnmax-N_DIRECT-RPC); ++i2) {
            if (check_cluster(iirefs[i2], -1, inodeidx) == false) {
                return false;
            }
            fret = soReadCacheCluster(iirefs[i2], irefs);
            if (fret < 0) FABORT(fret, "parse_cluster");
            for (i1 = 0; (i1 < RPC) && (i2*RPC+i1 < cnmax-N_DIRECT-RPC); ++i1) {
                if (check_cluster(irefs[i1], -1, inodeidx) == false) {
                    return false;
                }
            }
        }

    }

    return ret;
}

/* assumes inodes are sane */
testresult_t test_clusterused(void)
{
    testresult_t ret;
    ic_t icstat;
    SOSuperBlock *sb;
    SOInode inode;
    uint32_t r;

    fetch_superblock(&sb);
    ret = nice;

    for (r = 0; (r < sb->itotal) && (ret != corrupt); ++r) {
        ictable_get(r, &icstat);
        soReadInode(&inode, r);
        switch (icstat) {
            case idle:
                if ((inode.mode & INODE_TYPE_MASK & (!INODE_FREE)) != 0) {
                    /* inode is dirty - check */
                    if (parse_clusters(r) == false) ret = corrupt;
                }
                break;
            case busy:
                if (parse_clusters(r) == false) ret = corrupt;
                break;
            default:
                printf("BUM! test_clusterused assumes inode control has "
                       "only idle and busy in it by now.\n");
                ret = corrupt;
                break;
        }
    }

    return ret;
}
