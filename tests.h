#ifndef _TESTS_
#define _TESTS_

#include "fsck.h"

testresult_t test_sbmagic(void);
testresult_t test_sbname(void);
testresult_t test_sbzones(void);
testresult_t test_sbfctablesize(void);
testresult_t test_sbciutablesize(void);
testresult_t test_sbitotal(void);
testresult_t test_sbdzonesize(void);
testresult_t test_infree(void);
testresult_t test_inused(void);
testresult_t test_inlost(void);
testresult_t test_ininfo(void);
testresult_t test_clusterfree(void);
testresult_t test_clusterused(void);
testresult_t test_clusterlost(void);

#endif /* _TESTS_ */
