#include <stdio.h>

#include "sofs09.h"
#include "fsck.h"
#include "fsck_helper.h"

static bool check_cluster(uint32_t cba)
{
    uint32_t ci;
    cc_t ccstat;
    SOSuperBlock *sb;

    fetch_superblock(&sb);

    /* check if block address is inside data zone and if it is a valid
     * cluster address (alignment) */
    if (cba < (sb->dzone_start)                                         ||
        cba >= (sb->dzone_start) + (sb->dzone_size)*BLOCKS_PER_CLUSTER  ||
        ((cba-(sb->dzone_start))%BLOCKS_PER_CLUSTER) != 0)
    {
        printf("Member of free cluster list is not a valid cluster address, %u"
               ".\n", cba);
        return false;
    }

    /* check if cluster was already marked */
    ci = (cba-(sb->dzone_start))/BLOCKS_PER_CLUSTER;
    cctable_get(ci, &ccstat);
    if (ccstat != bah) {
        printf("Cluster number %u is used multiple times.\n", ci);
        return false;
    }

    /* mark cluster */
    cctable_set(ci, idle);

    return true;
}

testresult_t test_clusterfree (void)
{
    testresult_t ret;
    uint32_t fret;
    uint32_t freeclusters;
    uint32_t pos;
    uint32_t fcrefs[RPB];
    uint32_t cba; /* cluster block address */
    SOSuperBlock *sb;
    uint32_t cacheidx, cachecnt;
    uint32_t r;

    fetch_superblock(&sb);

    ret = nice;
    freeclusters = 0;

    /* check fctable limits */
    if ((sb->fctable_head) >= (sb->fctable_size)*RPB) {
        printf("fctable_head is out of bounds.\n");
        return corrupt;
    }
    if ((sb->fctable_tail) >= (sb->fctable_size)*RPB) {
        printf("fctable_tail is out of bounds.\n");
        return corrupt;
    }
    pos = sb->fctable_head;
    while (pos != sb->fctable_tail) {
        fret = soReadRawBlock((sb->fctable_start)+(pos/RPB), fcrefs);
        if (fret < 0) FABORT(fret, "sbfreeclusters");

        cba = fcrefs[pos%RPB];
        if (check_cluster(cba) == true) {
            freeclusters++;
        } else {
            printf("It was entry number %u of fctable.\n", pos);
            ret = corrupt;
        }
        pos = (pos+1)%((sb->fctable_size)*RPB);
    }

    /* check head cache */
    cacheidx = sb->dzone_head.cache_idx;
    cachecnt = sb->dzone_head.cache_cnt;
    if (cacheidx + cachecnt > DZONE_CACHE_SIZE) {
        printf("Head cache has incorrect information.\n");
        return corrupt;
    }
    for (r = cacheidx; r < cacheidx + cachecnt, ++r) {
        cba = (sb->dzone_head.cache)[r]; 
        if (check_cluster(cba) == true) {
            freeclusters++;
        } else {
            printf("It was entry number %u of dzone_head cache.\n", r);
            ret = corrupt;
        }
    }

    /* check tail cache */
    cacheidx = 0;
    cachecnt = sb->dzone_tail.cache_cnt;
    if (cacheidx + cachecnt >= DZONE_CACHE_SIZE) {
        printf("Tail cache has incorrect information.\n");
        return corrupt;
    }
    for (r = cacheidx; r < cacheidx + cachecnt; ++r) { 
        cba = (sb->dzone_tail.cache)[r]; 
        if (check_cluster(cba) == true) {
            freeclusters++;
        } else {
            printf("It was entry number %u of dzone_tail cache.\n", r);
            ret = corrupt;
        }
    }

    if (freeclusters != sb->dzone_free) {
        printf("donze_free is %u, should be %u.\n", sb->dzone_free,
               freeclusters);
        ret = corrupt;
    }

    return ret;
}
