#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

static bool parse_directory(uint32_t inodeidx)
{
    bool ret;
    uint32_t fret;
    uint32_t den, den_max, den_mod;
    SOInode inode, newinode;
    SODirEntry dec[DPC]; /* direntry cluster */
    ic_t icstat;
    SOSuperBlock *sb;

    fetch_superblock(&sb);

    ret = true;

    fret = soReadInode(&inode, inodeidx);
    if (fret < 0) FABORT(fret, "parse_directory");

    if ((inode.size % sizeof(SODirEntry)) != 0) {
        printf("Inode %d is a directory but its size is not multiple of "
               "sizeof(SODirEntry).\n", inodeidx);
        return false;
    }

    den_max = inode.size/sizeof(SODirEntry);
    for (den = 0; den < den_max; ++den) {
        /* read a new cluster when necessary */
        den_mod = den%DPC;
        if (den_mod == 0) {
            fret = soReadFileCluster(inodeidx, den/DPC, dec);
            if (fret < 0) FABORT(fret, "parse_directory");
        }
        /* process direntry */
        if (dec[den_mod].name[0] != '\0') {
            /* check validity of inode address */
            if (dec[den_mod].inode >= sb->itotal) {
                printf("Direntry number %d of inode %d is pointing to %d, "
                        "which lies outside inode table.\n", den,
                        inodeidx, dec[den_mod].inode);
                return false;
            }

            fret = soReadInode(&newinode, dec[den_mod].inode);
            if (fret < 0) FABORT(fret, "parse_directory");

            /* update tables */
            irtable_inc(dec[den_mod].inode);
            ictable_get(dec[den_mod].inode, &icstat);
            switch (icstat) {
                case bah:
                    ictable_set(dec[den_mod].inode, busy);
                    break;
                case idle:
                    printf ("Inode %d is marked as free but is in use.\n",
                            dec[den_mod].inode);
                    break;
                case busy:
                    /* already parsed this inode */
                    break;
                default:
                    printf("BUM! ictable has something strange in it.\n");
                    return false;
                    break;
            }

            /* don't parse direntries pointing to non directories, inodes
             * already parsed or this inode
             */
            if (((newinode.mode & INODE_DIR) == INODE_DIR) &&
                (icstat != busy)                           &&
                (dec[den_mod].inode != inodeidx))
            {
                ret = parse_directory(dec[den_mod].inode);
            }

        }
    }
            
    return ret;
}

testresult_t test_inused(void)
{
    testresult_t ret;

    ret = nice;

    /* begin in root directory */
    if (parse_directory(0) == false) {
        ret = corrupt;
    }

    return ret;
}
