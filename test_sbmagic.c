#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbmagic (void)
{
    testresult_t ret;
    SOSuperBlock *sb;

    fetch_superblock(&sb);

    if (sb->magic == MAGIC_NUMBER) {
        ret = nice;
    } else {
        ret = corrupt;
        printf("This is not a valid SOFS09 filesystem.\n");
    }

    return ret;
}
