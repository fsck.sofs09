#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_inlost(void)
{
    testresult_t ret;
    ic_t icstat;
    uint32_t r;
    SOSuperBlock *sb;

    fetch_superblock(&sb);

    ret = nice;

    for (r = 0; r < sb->itotal; ++r) {
        ictable_get(r, &icstat);
        if (icstat == bah) {
            printf("Inode number %d is lost.\n", r);
            ret = corrupt;
        }
    }

    return ret;
}
