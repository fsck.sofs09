#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbzones (void)
{
    testresult_t ret;
    int r, s;
    int nzones;
    SOSuperBlock *sb;

    fetch_superblock(&sb);

    /* current position inside the disc, in blocks */
    uint32_t cpos;
    
    struct zone_s {
        /* start position, in blocks, of this zone */
        uint32_t start;
        /* size, in blocks, of this zone */
        uint32_t size;
        /* zone name */
        char *name;
    };
    struct zone_s zone[] = {
        {sb->fctable_start, sb->fctable_size, "FCT"},
        {sb->ciutable_start, sb->ciutable_size, "CIUT"},
        {sb->itable_start, sb->itable_size, "IT"},
        {sb->dzone_start, sb->dzone_size*BLOCKS_PER_CLUSTER, "DATA"},
    };
    struct zone_s tempzone;

    ret = nice;

    /* sort zones by start position (bubble sort) */
    nzones = sizeof(zone)/sizeof(struct zone_s);
    for (r = 0; r < nzones-1; ++r) {
        for (s = r; s < nzones-1-r; ++s) {
            if (zone[r].start > zone[r+1].start) {
                tempzone = zone[r];
                zone[r] = zone[r+1];
                zone[r+1] = tempzone;
            }
        }
    }

    cpos = 0;

    /* advance superblock */
    cpos++;

    for (r = 0; (r < nzones) && (ret != corrupt); ++r) {
        if (cpos > zone[r].start) {
            printf("Zones overlap.\n");
            ret = corrupt;
        } else {
            if (cpos != zone[r].start) {
                printf("Zones are not contiguous.\n");
                cpos = zone[r].start;
                ret = weird;
            }
            cpos += zone[r].size;
            if (cpos > sb->ntotal) {
                printf("Zones exceed disc size.\n");
                ret = corrupt;
            }
        }
    }

    printf("Detected structure: ZONE_NAME (start,end)\n");
    printf("0: SB (0,1) : ");
    for (r = 0; r < nzones; ++r) {
        printf("%s (%d,%d): ", zone[r].name, zone[r].start,
               zone[r].start + zone[r].size);
    }
    printf("%d\n", sb->ntotal);

    return ret;
}

