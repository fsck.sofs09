/* This module implements functions that allow a controlled access to vital
 * information of fsck.
 *
 * No function from this module returns if failure occurs.
 * When something wrong occurs, the function aborts the program, immediately.
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include "fsck_helper.h"
#include "sofs09.h"

void printError (int errcode, char *cmd_name)
{
    fprintf(stderr, "%s: error #%d: %s\n", cmd_name, errcode,\
            strerror(errcode));
}

static const int NCOLS = 80;

/* Fetch current disc superblock.
 * If the superblock was not read, it is read from disk.
 * No changes to the superblock are monitored. It is assumed the superblock is
 * used as read-only.
 *  
 * argument sb is the address of a pointer to a SOSuperBlock
 *  
 * The address to the superblock is placed in (*sb).
 */     
void fetch_superblock(SOSuperBlock **sb)
{       
    assert(&(*sb) != NULL);

    static SOSuperBlock localsb;
    static bool localsb_valid = 0;
    int fret;

    if (localsb_valid == false) {
        fret = soReadRawBlock(0, &localsb);
        if (fret < 0) FABORT(fret, "fetch_superblock");
        localsb_valid = true;
    }

    (*sb) = &localsb;
}

/* Inode control table */

static ic_t *ictable;
static bool ictable_valid = false;
static uint32_t ictable_size = 0;

void ictable_free(void)
{
    assert(ictable_valid == true);

    free(ictable);
    ictable_valid = false;
}

void ictable_create(void)
{
    assert (ictable_valid == false);

    int r;
    SOSuperBlock *sb;
    fetch_superblock(&sb);

    ictable = malloc(sizeof(ic_t)*(sb->itotal));
    if (ictable == NULL) FABORT(errno, "ictable_init");

    ictable_size = sb->itotal;
    for (r = 0; r < ictable_size; ++r) {
        ictable[r] = bah;
    }

    ictable_valid = true;
}

void ictable_set(uint32_t index, ic_t value)
{
    assert(ictable_valid == true);
    assert(index < ictable_size);

    ictable[index] = value;
}

void ictable_get(uint32_t index, ic_t *value)
{
    assert(value != NULL);
    assert(ictable_valid == true);
    assert(index < ictable_size);

    (*value) = ictable[index];
}

/* Cluster control table */
static cc_t *cctable;
static bool cctable_valid = false;
static uint32_t cctable_size = 0;

void cctable_free(void)
{
    assert(cctable_valid == true);

    free(cctable);
    cctable_valid = false;
}

void cctable_create(void)
{
    assert (cctable_valid == false);

    int r;
    SOSuperBlock *sb;
    fetch_superblock(&sb);

    cctable = malloc(sizeof(cc_t)*(sb->dzone_size));
    if (cctable == NULL) FABORT(errno, "cctable_init");

    cctable_size = sb->dzone_size;
    for (r = 0; r < cctable_size; ++r) {
        cctable[r] = bah;
    }

    cctable_valid = true;
}

void cctable_set(uint32_t index, cc_t value)
{
    assert(cctable_valid == true);
    assert(index < cctable_size);

    cctable[index] = value;
}

void cctable_get(uint32_t index, cc_t *value)
{
    assert(value != NULL);
    assert(cctable_valid == true);
    assert(index < cctable_size);

    (*value) = cctable[index];
}

void cctable_print(void)
{
    assert(cctable_valid == true);

    int ncols;
    int r;
    cc_t ccstat;

    ncols = NCOLS;

    printf("\nCluster control table contents ([42mfree[m [44mbusy[m "
           "[41munknown[m)\n");
    for (r = 0; r < cctable_size; ++r) {
        cctable_get(r, &ccstat);
        switch (ccstat) {
            case idle:
                printf("[42m ");
                break;
            case busy:
                printf("[44m ");
                break;
            case bah:
                printf("[41m ");
                break;
        }
        if (((r+1)%ncols) == 0) printf("[m\n");
    }
    printf("[m\n");
}

/* inode refcount table */

static uint32_t *irtable;
static bool irtable_valid = false;
static uint32_t irtable_size = 0;

void irtable_free(void)
{
    assert(irtable_valid == true);

    free(irtable);
    irtable_valid = false;
}

void irtable_create(void)
{
    assert (irtable_valid == false);

    int r;
    SOSuperBlock *sb;
    fetch_superblock(&sb);

    irtable = malloc(sizeof(uint32_t)*(sb->itotal));
    if (irtable == NULL) FABORT(errno, "irtable_init");

    irtable_size = sb->itotal;
    for (r = 0; r < irtable_size; ++r) {
        irtable[r] = 0;
    }

    irtable_valid = true;
}

void irtable_inc(uint32_t index)
{
    assert(irtable_valid == true);
    assert(index < irtable_size);

    irtable[index]++;
}

void irtable_get(uint32_t index, uint32_t *value)
{
    assert(value != NULL);
    assert(irtable_valid == true);
    assert(index < irtable_size);

    (*value) = irtable[index];
}

void itable_print(void)
{
    assert(ictable_valid == true);

    int ncols;
    int r;
    ic_t icstat;
    uint32_t refcount;

    ncols = NCOLS;

    printf("\nInode control table contents ([42mfree[m [44mbusy[m "
           "[41munknown[m)\n");
    for (r = 0; r < ictable_size; ++r) {
        ictable_get(r, &icstat);
        irtable_get(r, &refcount);
        switch (icstat) {
            case idle:
                printf("[42m%d", refcount);
                break;
            case busy:
                printf("[44m%d", refcount);
                break;
            case bah:
                printf("[41m%d", refcount);
                break;
            default:
                printf("[m\nBUM!\n");
        }
        if (((r+1)%ncols) == 0) printf("[m\n");
    }
    printf("[m\n");
}

