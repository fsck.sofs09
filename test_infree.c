#include <stdio.h>
#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_infree (void)
{
    testresult_t ret;
    uint32_t fret;
    SOSuperBlock *sb;
    uint32_t ifree;
    SOInode inode;
    uint32_t inodeidx, previnodeidx;
    ic_t icstat;

    ret = nice;

    fetch_superblock(&sb);

    ifree = 0;
    inodeidx = sb->ihead;
    previnodeidx = NULL_INODE;
    while (inodeidx != NULL_INODE) {
        if (inodeidx < sb->itotal) {
            ictable_get(inodeidx, &icstat);
            switch (icstat) {
                case bah:
                    fret = soReadInode(&inode, inodeidx);
                    if (fret < 0) FABORT(fret, "test_infree");
                    ifree++;
                    ictable_set(inodeidx, idle);
                    break;
                case busy:
                    printf("Inode number %u is marked as used and is in free"
                           "inode list.\n", inodeidx);
                    ret = corrupt;
                    break;
                case idle:
                    printf("Free inode list closes over itself.\n");
                    return corrupt;
                    break;
                default:
                    printf("BUM! ictable has something strange in it.\n");
                    return corrupt;
                    break;
            }
        } else {
            printf("Free inode list has a member pointing to %u, which is "
                   "outside the list.\n", inodeidx);
            return corrupt;
        }

        /* check previous */
        if (inode.prev != previnodeidx) {
            printf("Prev of inode %u in free inode list is %u, should be %u.\n"
                    inodeidx, inode.prev, previnodeidx);
            ret = corrupt;
        }

        previnodeidx = inodeidx;
        inodeidx = inode.next;
    }

    /* check itail */
    if (previnodeidx != sb->itail) {
      	printf("Itail is %u, should be %u.\n", sb->itail, previnodeidx);
        ret = corrupt;
    }
    if (ifree != sb->ifree) {
        printf("ifree is %u, should be %u.\n", sb->ifree, ifree);
        ret = corrupt;
    }

    return ret;
}
