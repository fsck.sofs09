#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbname (void)
{
    testresult_t ret;
    SOSuperBlock *sb;
    int r;

    fetch_superblock(&sb);

    ret = corrupt;
    for (r = 0; (r < PARTITION_NAME_SIZE) && (ret == corrupt); ++r) {
        if ((sb->name)[r] == '\0') {
            ret = nice;
        }
    }

    if (ret == corrupt) {
        printf("Partition name is not correctly terminated.\n");
    } else {
        printf("Partition name: %s.\n", sb->name);
    }

    return ret;
}
