#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/* open and close disc */
#include "sofs_rawdisc.h"
#include "fsck.h"
#include "fsck_helper.h"
#include "tests.h"

struct test_s {
    testfunction_t function;
    char *name;
};
typedef struct test_s test_t;

void main (int argc, char *argv[])
{
    int ret;
    int fret;
    char *disc_path;
    testresult_t testres;
    int r;
    int nfuncs;

    /* functions containing the tests to be made */
    test_t test[] =
    {
        {test_sbmagic, "sbmagic"},
        {test_sbname, "sbname"},
        {test_sbzones, "sbzones"},
        {test_sbfctablesize, "sbfctablesize"},
        {test_sbciutablesize, "sbciutablesize"},
        {test_sbitotal, "sbitotal"},
        {test_sbdzonesize, "sbdzonesize"},
        {test_infree, "infree"},
        {test_inused, "inused"},
        {test_inlost, "inlost"},
        {test_ininfo, "ininfo"},
        {test_clusterfree, "clusterfree"},
        {test_clusterused, "clusterused"},
        {test_clusterlost, "clusterlost"},
    };

    if (argc != 2) ABORT(-EINVAL);
    nfuncs = sizeof(test)/sizeof(test_t);
    disc_path = argv[1];

    printf("Opening file %s as disc.\n", disc_path);
    fret = soOpenDevice(disc_path);
    if (fret < 0) ABORT(fret);
    atexit((void *)soCloseDevice);

    /* create internal structures */
    cctable_create();
    atexit(cctable_free);
    ictable_create();
    atexit(ictable_free);
    irtable_create();
    atexit(irtable_free);
    
    ret = EXIT_SUCCESS;
    for (r = 0; (r < nfuncs) && (ret == EXIT_SUCCESS); ++r) {
        testres = test[r].function();
        switch (testres) {
            case corrupt:
                printf("System file corrupted.\n");
                ret = EXIT_FAILURE;
                break;
            case weird:
                printf("System file is weird.\n");
                break;
            case nice:
                printf("Test %s passed with success.\n", test[r].name);
                break;
            default:
                printf("BUM!\n");
                ret = EXIT_FAILURE;
                break;
        }
    }

    cctable_print();
    itable_print();

    exit(ret);
}

