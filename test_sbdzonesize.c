#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbdzonesize (void)
{
    testresult_t ret;
    SOSuperBlock *sb;

    fetch_superblock(&sb);
    
    /* ceil(a/b) = (a + b - 1)/b */

    ret = nice;

    if (sb->dzone_size == 0) {
        printf("Data zone is empty.\n");
        ret = corrupt;
    }

    if (sb->dzone_size == 1) {
        printf("Data zone can only store the root directory.\n");
        ret = weird;
    }

    return ret;
}
