#ifndef _FSCK_
#define _FSCK_

typedef enum {corrupt, weird, nice} testresult_t;
typedef testresult_t (*testfunction_t)(void);

#endif /* _FSCK_ */
