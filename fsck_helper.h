#ifndef _FSCK_HELPER_
#define _FSCK_HELPER_

#include "stdlib.h"
#include "sofs_superblock.h"

/* usefull macro and function */

void printError (int errcode, char *cmd_name);

#define ABORT(err) \
    {\
        printError(-(err), argv[0]);\
        exit(EXIT_FAILURE);\
    }

#define FABORT(err, fname) \
    {\
        printError(-(err), fname);\
        exit(EXIT_FAILURE);\
    }

typedef enum{idle, busy, bah} ic_t;
/* C is stupid */
//typedef enum{idle, busy, bah} cc_t;
typedef ic_t cc_t;

void fetch_superblock(SOSuperBlock **sb);

/* inode control table */
/* index < itotal */
void ictable_create(void);
void ictable_free(void);
void ictable_set(uint32_t index, ic_t value);
void ictable_get(uint32_t index, ic_t *value);

/* cluster control table */
/* index < dzone_size */
void cctable_create(void);
void cctable_free(void);
void cctable_set(uint32_t index, cc_t value);
void cctable_get(uint32_t index, cc_t *value);
void cctable_print(void);

/* inode refcount table */
/* index < itotal */
void irtable_create(void);
void irtable_free(void);
void irtable_inc(uint32_t index);
void irtable_get(uint32_t index, uint32_t *value);

void itable_print(void);

#endif /* _FSCK_HELPER_ */
