#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbciutablesize (void)
{
    testresult_t ret;
    SOSuperBlock *sb;
    int ciutshortage, ciutexcess;

    fetch_superblock(&sb);
    
    /* ceil(a/b) = (a + b - 1)/b */

    ret = nice;

    if (sb->ciutable_size < (sb->dzone_size+RPB-1)/RPB) {
        ciutshortage = (sb->dzone_size+RPB-1)/RPB - (sb->ciutable_size);
        printf("CIUT too small by %d blocks.\n", ciutshortage);
        ret = corrupt;
    }

    if (sb->ciutable_size > (sb->dzone_size+RPB-1)/RPB) {
        ciutexcess = (sb->ciutable_size) - (sb->dzone_size+RPB-1)/RPB;
        printf("CIUT too big by %d blocks.\n", ciutexcess);
        ret = weird;
    }

    return ret;
}
