#include "sofs09.h"
#include "fsck.h"
#include "fsck_helper.h"

/* This test can only be run after all other inode tests.
 * This test is more like a set of tests, so, a lot of returns are spread all
 * over the code.
 */
testresult_t test_ininfo(void)
{
    testresult_t ret;
    int inodeidx;
    SOInode inode;
    ic_t icstat;
    int typesum;
    uint32_t realrefcount;
    SOSuperBlock *sb;
    int fret;

    ret = nice;
    fetch_superblock(&sb);

    for (inodeidx = 0; inodeidx < sb->itotal; ++inodeidx) {
        fret = soReadInode(&inode, inodeidx);
        if (fret < 0) FABORT(fret, "test_ininfo");

        /* check freeness */
        ictable_get(inodeidx, &icstat);
        switch(icstat) {
            case idle:
                if ((inode.mode && INODE_FREE) == 0) {
                    printf("Inode %d is free, but it's INODE_FREE bit is not "
                           "set.\n", inodeidx);
                    ret = corrupt;
                }
                break;
           case busy:
                if ((inode.mode && INODE_FREE) == INODE_FREE) {
                    printf("Inode %d is in use, but it's INODE_FREE bit is "
                           "set.\n", inodeidx);
                    ret = corrupt;
                }
                break;
           default:
                printf("BUM! test_ininfo assumes inodes are either free or "
                       "used.\n");
                return corrupt;
        }

        /* check type */
        typesum = (inode.mode && INODE_DIR)/INODE_DIR +
                  (inode.mode && INODE_FILE)/INODE_FILE +
                  (inode.mode && INODE_SYMLINK)/INODE_SYMLINK;
        if (((inode.mode && INODE_FREE) == 0) && typesum == 0) {
            printf("Inode %d is not free but has no type.\n", inodeidx);
            ret = corrupt;
        }
        if (typesum > 1) {
            printf("Inode %d has many types.\n", inodeidx);
            ret = corrupt;
        }

        /* check refcount */
        irtable_get(inodeidx, &realrefcount);
        if (realrefcount != inode.refcount) {
            printf("Refcount of inode %d is %d but should be %d.\n", inodeidx,
            inode.refcount, realrefcount);
            ret = corrupt;
        }
    }

    return ret;
}
