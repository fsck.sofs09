#include <stdio.h>

#include "fsck.h"
#include "fsck_helper.h"
#include "sofs09.h"

testresult_t test_sbfctablesize (void)
{
    testresult_t ret;
    SOSuperBlock *sb;
    int fctshortage, fctexcess;

    fetch_superblock(&sb);
    
    /* ceil(a/b) = (a + b - 1)/b */

    ret = nice;

    if (sb->fctable_size < ((sb->dzone_size-1)+RPB-1)/RPB) {
        fctshortage = (sb->dzone_size+RPB-1)/RPB - (sb->fctable_size);
        printf("FCT too small (even considering that the root directory block "
               "will never be there) by %d blocks.\n", fctshortage);
        ret = corrupt;
    }

    if (sb->fctable_size > (sb->dzone_size+RPB-1)/RPB) {
        fctexcess = (sb->fctable_size) - (sb->dzone_size+RPB-1)/RPB;
        printf("FCT too big by %d blocks.\n", fctexcess);
        ret = weird;
    }

    return ret;
}
